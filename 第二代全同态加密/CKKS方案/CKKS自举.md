## CKKS自举系列的文章
- [CHKKS17] [Bootstrapping for Approximate Homomorphic Encryption](https://eprint.iacr.org/2018/153.pdf)
- [CCS18] [Improved Bootstrapping for Approximate Homomorphic Encryption](https://eprint.iacr.org/2018/1043.pdf)
